import * as Koa from "koa"
import * as Cors from "@koa/cors"
import * as KoaRouter from "koa-router"
import {Context} from "koa"
import * as KoaLogger from "koa-logger"
import * as KoaStatic from "koa-static"

import ytdl = require("ytdl-core")

async function downloadVideo(ctx: Context) {
    const videoURL: string = "https://www.youtube.com/watch?v=" + ctx.params.videoID

    ctx.response.set("Content-Type", 'video/mp4')
    ctx.response.set("Content-Disposition", "attachment; filename=\"video.mp4\"")

    ctx.response.body = ytdl(videoURL, { filter: (format) => format.container === 'mp4' })
    ctx.response.status = 200
}

const router: KoaRouter = new KoaRouter().prefix("/download").get("/:videoID", downloadVideo)

async function startServer(port: number) {
    const app: Koa = new Koa()

    app.use(Cors())
        .use(KoaLogger())
        .use(router.routes())
        .use(router.allowedMethods())
        .use(KoaStatic("front"))
        .listen(port)
}

startServer(3000)
    .then(() => console.log("Jacques chirac est mort pour nos péchés."))
    .catch((err: Error) => console.log("error '" + err.name + "' " + err.message))