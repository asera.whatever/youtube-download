"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const Koa = require("koa");
const Cors = require("@koa/cors");
const KoaRouter = require("koa-router");
const KoaLogger = require("koa-logger");
const KoaStatic = require("koa-static");
const ytdl = require("ytdl-core");
function downloadVideo(ctx) {
    return __awaiter(this, void 0, void 0, function* () {
        const videoURL = "https://www.youtube.com/watch?v=" + ctx.params.videoID;
        ctx.response.set("Content-Type", 'video/mp4');
        ctx.response.set("Content-Disposition", "attachment; filename=\"video.mp4\"");
        ctx.response.body = ytdl(videoURL, { filter: (format) => format.container === 'mp4' });
        ctx.response.status = 200;
    });
}
const router = new KoaRouter().prefix("/download").get("/:videoID", downloadVideo);
function startServer(port) {
    return __awaiter(this, void 0, void 0, function* () {
        const app = new Koa();
        app.use(Cors())
            .use(KoaLogger())
            .use(router.routes())
            .use(router.allowedMethods())
            .use(KoaStatic("front"))
            .listen(port);
    });
}
startServer(3000)
    .then(() => console.log("Jacques chirac est mort pour nos péchés."))
    .catch((err) => console.log("error '" + err.name + "' " + err.message));
//# sourceMappingURL=index.js.map